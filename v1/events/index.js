const style = 'style';
const src = 'src/*';

const gulp = require('gulp');
const sass = require('gulp-ruby-sass');

gulp.task('sass', () =>
    sass(src)
        .on('error', sass.logError)
        .pipe(gulp.dest(style))
);

gulp.task('default', () => {
    return gulp.watch(src,['sass']);
});
