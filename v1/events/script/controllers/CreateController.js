app.controller("CreateController", function( $scope, newActivity, ProviderService, $state) {
  $scope.item = newActivity;
  /**
   * f0 to f3 are flat to controller the possibility to save. 
   * The first 3 controller if the input are empty. 
   * The last control that the hour of start be first that the hour of ends 
   */ 
  var f0=0,f1=0,f2=0,f3=0;
  var start, ends, diff;
  var ctrl = this;
  $scope.flat = true;
  $scope.alert = false;
  $scope.available = function(m,i){
    switch(i) {
      case 0:
        if (m !== undefined){
          f0 = 1;
          start = moment(m, "hh:mm a");
        }
        break;
      case 1:
        if (m !== undefined){
          f1 = 1;
          ends = moment(m, "hh:mm a");
          if (f0 == 1 ){
            diff = ends.diff(start);
            if (diff >= 0){
              f3 = 1;
              $scope.alert=false;
            } else{
              f3 = 0;
              $scope.alert=true;
            }
          }
        }
        break;
        case 2:
        if (m !== undefined){
          f2 = 1;
        }
        break;
      }
      if (f0*f1*f2*f3 != 0){
        $scope.flat = false;
      } else{
        $scope.flat = true;
      } 
  }

  $scope.set = function (m) {
    if (m === undefined){
      return $scope.item.day;
    }
    return $scope.ctrl.day = moment(m,"MM-DD-YYYY").format("dddd");
  };

  $scope.save = function(item, ctrl){
    item.date = ctrl.date;
    item.day = ctrl.day;
    item.start = ctrl.start;
    item.ends = ctrl.ends;
    ProviderService.newActivity(item);
    return $state.go('master');
  }; 
});
