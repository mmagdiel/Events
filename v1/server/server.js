var express = require('express');
var json = require('json-server'); 
var app = express();

app.use('/api', json.router('db.json'));
app.use('/img', express.static( __dirname + '/../fuckton_events/img'));
app.use('/fonts', express.static( __dirname + '/../fuckton_events/fonts'));
app.use('/style', express.static( __dirname + '/../fuckton_events/style'));
app.use('/views', express.static( __dirname + '/../fuckton_events/views'));
app.use('/script', express.static( __dirname + '/../fuckton_events/script'));
app.use('/script/libs', express.static( __dirname + '/../fuckton_events/scriptlibs'));
app.use('/script/services', express.static( __dirname + '/../fuckton_events/script/services'));
app.use('/script/controllers', express.static( __dirname + '/../fuckton_events/script/controllers'));

app.all('/*', function(req, res, next) {
    // Just send the index.html for other files to support HTML5Mode
    res.sendFile('index.html', { root: __dirname + '/../fuckton_events' });
});

app.listen(9356, () => {
  console.log('JSON Server is running on port 9356')
});