app.service("ProviderService", ProviderService);

function ProviderService($rootScope, $http, $state){
	var self = this;
	var location = document.location.origin;

	/**
	 * Get the list of all events
	 * @return {Promise} The result of querying the list of events
	 */
	this.explorer = function(){
		return $http.get(location + '/api/essentials?region_like=Shenzhen&_embed=events')
		.then(function onSuccess(response){
			console.log('all done');
			return response.data;
		}).catch(function onError(response){
		  console.log(response);
		});
	};


	/**
	 * Get event data from the provided id
	 * @param  {String}  id The id of the event document
	 * @return {Promise}    The result of the event query search
	 */
	this.timed = function(id, element){
		return $http.get(location + '/api/events/' + id + '?_expand=essential&_expand=time')
		.then(function onSuccess(response){
			console.log('one done', response);
			return response.data;
		}).catch(function onError(response){
			console.log(response);
		});
	};


	/**
	 * Get the list of all events
	 * @return {Promise} The result of querying the list of events
	 this.activities = function(){
		 return $http.get(location + '/api/events')
		 .then(function onSuccess(response){
			 console.log('all done');
			 return response.data; 
			}).catch(function onError(response){
				console.log(response);
			});
		};
	*/
		
	/**
	 * Get event data from the provided id
	 * @param  {String}  id The id of the event document
	 * @return {Promise}    The result of the event query search
	 */
	this.activity = function(id){
    return $http.get(location + '/api/events/' + id+ '?_expand=essential&_expand=time&_expand=device')
      .then(function onSuccess(response){
			console.log('one done', response.data);
			return response.data;
      }).catch(function onError(response){
        console.log(response);
      });
	};

	/**
	 * Create event from the data send
	 * @return {Promise} The result of the event created
	 */
	this.newActivity = function(data){
		return $http({
			method: 'POST',
			url: location + '/api/events',
			data: data,
			headers: { 'Content-Type': 'application/json' }
		}).then(function onSuccess(response){
			return console.log("newOne done");
		}).catch(function onError(response){
			console.log(response);
		});
	};	
	
	/**
	 * Update the event data from the provided id
	 * @param  {String}  id The id of the event document
	 * @return {Promise}    The result of the event query search
	 */
	this.reActivity = function(id, data){
		return  $http({
			method: 'PUT',
			url: location + '/api/events/' + id,
			data: data,
			headers: { 'Content-Type': 'application/json' }
		}).then(function onSuccess(response){
			return console.log("reOne done");;
		}).catch(function onError(response){
			console.log(response);
		});
	};
	
	/**
	 * Delete a event data from the provided id
	 * @param  {String}  id The id of the event document
	 * @return {Promise}    The result of the event delete
	 */
	this.unActivity = function(id){
		return $http({
			method: 'DELETE',
			url: location + '/api/events/' + id,
			headers: { 'Content-Type': 'application/json' }
			}).then(function onSuccess(response){
				return console.log("unOne done");
      }).catch(function onError(response){
        console.log(response);
      });
	};

	/**
	 * Get the list of all users
	 * @return {Promise} The result of querying the list of users
	 */
	this.people = function(){
		return $http.get(location + '/api/users')
		.then(function onSuccess(response){
				console.log('all done');
				return response.data; 
		}).catch(function onError(response){
		  console.log(response);
		});
	};

	/**
	 * Get the details of a user
	 * @return {Promise} The result of querying the user's detail 
	 */
	this.user = function(id){
		return $http.get(location + '/api/users/' + id)
		.then(function onSuccess(response){
				console.log('one done');
				return response.data; 
		}).catch(function onError(response){
		  console.log(response);
		});
	};

	/**
	 * Get the details of a user' created
	 * @return {Promise} The result of querying the user's created
	 */
	this.newUser = function(id, data){
		return $http({
			method: 'POST',
			url: location + '/api/users/' + id,
			data: data,
			headers: { 'Content-Type': 'application/json' }
		}).then(function onSuccess(response){
			return console.log("newUser done");
		}).catch(function onError(response){
			console.log(response);
		});
	};

	/**
	 *  users/0?_expand=events
	 * Get the list of all plans
	 * @return {Promise} The result of querying the list of plans
	 */
	this.plans = function(id){
		return $http.get(location + '/api/users/' + id + '?_expand=events')
		.then(function onSuccess(response){
				console.log('all done');
				return response.data; 
		}).catch(function onError(response){
		  console.log(response);
		});
	};

	/**
	 * Get the plan's created
	 * @return {Promise} The result of querying of plan's created
	 */
	this.newPlan = function(id, data){
		return $http({
			method: 'POST',
			url: location + '/api/plan/' + id,
			data: data,
			headers: { 'Content-Type': 'application/json' }
		}).then(function onSuccess(response){
			return console.log("newplan done");
		}).catch(function onError(response){
			console.log(response);
		});
	};
}