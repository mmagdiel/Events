app.controller("DeleteController", function( $scope, $state, ProviderService, ngDialog ) {
	$scope.cancel = function() {
		return ngDialog.close();
	};
	$scope.sure = function(m) {
		ProviderService.unActivity(m.id);
		ngDialog.close();
		return $state.reload();
	}; 
});